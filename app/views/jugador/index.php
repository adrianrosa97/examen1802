<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Lista de Jugadores</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Nacimiento</th>
            <th>Puesto</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($jugadores as $j): ?>
            <tr>
              <td><?php echo $j->id ?></td>
              <td><?php echo $j->nombre ?></td>
              <td><?php echo date("d-m-Y", strtotime($j->nacimiento)) ?></td>
              <td><?php echo $j->puesto->nombre ?></td>
              <td><a class="btn btn-primary" href="/jugador/titular/<?php echo $j->id ?>">Titular</a>
             </td>
           </tr>
         <?php endforeach ?>
       </tbody>
     </table>
     <hr>
     Paginas:
     <?php for ($i = 1;$i <= $pages;$i++){ ?>
     <?php if ($i != $page): ?>
     <a href="/jugador/index?page=<?php echo $i ?>" class="btn">
      <?php echo $i ?>
      </a>
   <?php else: ?>
    <span class="btn">
      <?php echo $i ?>
    </span>
  <?php endif ?>
  <?php } ?>
  <hr>
  <a href="/register">Nuevo Jugador</a>
</div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>