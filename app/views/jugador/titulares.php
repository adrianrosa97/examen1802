<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Titulares</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Nacimiento</th>
            <th>Puesto</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($titulares as $t): ?>
            <tr>
              <td><?php echo $t->nombre ?></td>
              <td><?php echo date("d-m-Y", strtotime($t->nacimiento)) ?></td>
              <td><?php echo $t->puesto->nombre ?></td>
              <td><a class="btn btn-primary" href="/jugador/quitar/<?php echo $t->id ?>">Quitar</a>
             </td>
           </tr>
         <?php endforeach ?>
       </tbody>
     </table>
</div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>