<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Nuevo Jugador</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
      <h3>Nuevo jugador</h3>
        <form action="/register/store" method="post">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group">
                <label for="nacimiento">Nacimiento:</label>
                <input type="date" class="form-control" name="nacimiento" placeholder="YYYY-mm-dd">
            </div>
            <div class="form-group">
                <label for="puesto">Puesto:</label>
                <select name="puesto" class="form-control">
                <?php foreach ($puestos as $p): ?>
                    <option value="<?php echo $p->id ?>"><?php echo $p->nombre ?></option>
                <?php endforeach ?>
                </select>
            </div>
            <h3 style="color: red;"><?php echo isset($message) && !empty($message) ? $message : "" ?></h3>
            <button type="submit" class="btn btn-default">Crear</button>
        </form>
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
