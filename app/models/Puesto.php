<?php
namespace App\Models;

use PDO;
use \Core\Model;

require_once "../core/Model.php";
/**
*
*/
class Puesto extends Model
{

    function __construct()
    {

    }
    public static function all(){

        $db = Puesto::db();

        $statement = $db->query('SELECT * FROM puestos');
        $puestos = $statement->fetchAll(PDO::FETCH_CLASS,Puesto::class);
        return $puestos;
    }

    public function jugadores()
    {
        //un producto pertenece a un tipo:
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE id_puesto = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class)[0];

        return $jugador;
    }

    public static function find($id){
        $db = Puesto::db();

        $statement = $db->prepare("SELECT * FROM puestos WHERE id=:id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, Puesto::class);
        $puesto = $statement->fetch(PDO::FETCH_CLASS);
        return $puesto;
    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }
}