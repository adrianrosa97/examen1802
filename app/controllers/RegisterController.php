<?php

namespace App\Controllers;
// require_once "../app/models/User.php";
use \App\Models\Puesto;
use \App\Models\Jugador;
    /**
    *
    */
    class RegisterController
    {

        function __construct()
        {
        }

        public function index(){

            $puestos = Puesto::all();
            require "../app/views/register/index.php";
        }

        public function store(){

            if(isset($_REQUEST["nombre"]) && isset($_REQUEST["nacimiento"]) && isset($_REQUEST["puesto"])){
                $j = new Jugador();
                $j->nombre = $_REQUEST["nombre"];
                $j->nacimiento = date("Y/m/d", strtotime($_REQUEST["nacimiento"]));
                $j->id_puesto = (Integer)$_REQUEST["puesto"];
                $j->insert();

                header("Location:/jugador");
            }else{
                $message = "Debes rellenar todos los campos.";
                require "../app/views/register/index.php";
            }
        }
    }