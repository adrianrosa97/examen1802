<?php
namespace App\Controllers;
// require_once "../app/models/User.php";
use \App\Models\Jugador;
    /**
    *
    */
    class JugadorController
    {

        function __construct()
        {
        }

        public function index(){
            $numero = 5;
            $jugadores = Jugador::paginate($numero);
            $rowCount = Jugador::rowCount();

            $pages = ceil($rowCount / $numero);
            isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;
            require "../app/views/jugador/index.php";
        }

        public function show($args){
            $id = (int)$args[0];
            $user = User::find($id);
            require "../app/views/user/show.php";
        }

        public function create(){
            require "../app/views/user/create.php";
        }

        public function titular($args){
            $id = (int)$args[0];
            $jugador = Jugador::find($id);
            if(isset($_SESSION["titulares"])){
                $titulares = $_SESSION["titulares"];
            }else{
                $titulares = [];
            }
            
            if(isset($titulares[$jugador->id])){
                header("Location:/jugador");
                return;
            }
            $titulares[$jugador->id] = $jugador;
            $_SESSION["titulares"] = $titulares;

            header("Location:/jugador/titulares");
        }

        public function titulares(){
            if(isset($_SESSION["titulares"])){
                $titulares = $_SESSION["titulares"];
            }else{
                $titulares = [];
            }
            require "../app/views/jugador/titulares.php";
        }

        public function quitar($args){
            $id = (int)$args[0];
            $titulares = $_SESSION["titulares"];

            unset($titulares[$id]);

            $_SESSION["titulares"] = $titulares;
            
            require "../app/views/jugador/titulares.php";
        }
    }